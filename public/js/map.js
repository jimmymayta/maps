let lat = document.getElementById('lat');
let lng = document.getElementById('lng');
let zoom = document.getElementById('zoom');

let northEast_lat = document.getElementById('northEast_lat');
let northEast_lng = document.getElementById('northEast_lng');
let southWest_lat = document.getElementById('southWest_lat');
let southWest_lng = document.getElementById('southWest_lng');

var map = L.map("map").setView([-16.5022, -68.1259], 12);

L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
  attribution: "",
}).addTo(map);

var marker;

map.on("click", (e) => {
  console.log(map.getBounds())
  if (marker) {
    map.removeLayer(marker);
  }
  marker = L.marker([e.latlng.lat, e.latlng.lng]).addTo(map);
  lat.value = e.latlng.lat;
  lng.value = e.latlng.lng;
  zoom.value = e.target._zoom;

  northEast_lat.value = map.getBounds().getNorth();
  northEast_lng.value = map.getBounds().getEast();
  southWest_lat.value = map.getBounds().getSouth();
  southWest_lng.value = map.getBounds().getWest();
});


