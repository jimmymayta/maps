<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <link rel="stylesheet" href="css/leaflet.css"/>
  <link rel="stylesheet" href="css/map.css"/>
  <script src="js/leaflet.js"></script>
</head>
<body>
  <h1>Crear Mapa</h1>
  <a href="<?= route_to('mapas') ?>">Mapas</a>
  <br><br>
  <form action="<?= route_to('guardar') ?>" method="post">
    <input id="lat" type="hidden" name="lat" value="">
    <input id="lng" type="hidden" name="lng" value="">
    <input id="zoom" type="hidden" name="zoom" value="">

    <input id="northEast_lat" type="hidden" name="northEast_lat" value="">
    <input id="northEast_lng" type="hidden" name="northEast_lng" value="">
    <input id="southWest_lat" type="hidden" name="southWest_lat" value="">
    <input id="southWest_lng" type="hidden" name="southWest_lng" value="">

    <div id="map" style="border: 1px solid black"></div>
    <button type="submit">Guardar</button>
  </form>
  <script src="js/map.js"></script>
</body>
</html>
