<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Mapas</h1>
    <a href="<?= route_to('crearmapa') ?>">Crear Mapa</a>
    <br><br>
    <?php foreach ($query->getResult() as $item) : ?>
      <h1>Mapa <?= $item->id ?></h1>
      <input id="lat" type="hidden" name="lat" value="<?= $item->lat ?>">
      <input id="lng" type="hidden" name="lng" value="<?= $item->lng ?>">
      <input id="zoom" type="hidden" name="zoom" value="<?= $item->zoom ?>">
      <input id="northEast_lat" type="hidden" name="northEast_lat" value="<?= $item->northEast_lat ?>">
      <input id="northEast_lng" type="hidden" name="northEast_lng" value="<?= $item->northEast_lng ?>">
      <input id="southWest_lat" type="hidden" name="southWest_lat" value="<?= $item->southWest_lat ?>">
      <input id="southWest_lng" type="hidden" name="southWest_lng" value="<?= $item->southWest_lng ?>">

      <iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=<?= $item->northEast_lng ?>%2C<?= $item->northEast_lat ?>%2C<?= $item->southWest_lng ?>%2C<?= $item->southWest_lat ?>&amp;layer=mapnik&amp;marker=<?= $item->lat ?>%2C<?= $item->lng ?>" style="border: 1px solid black">
      </iframe>
      <a href="<?= route_to('editar', $item->id) ?>">editar</a>
      <a href="<?= route_to('eliminar', $item->id) ?>">eliminar</a>

      <br><br>
    <?php endforeach ?>
</body>
</html>
